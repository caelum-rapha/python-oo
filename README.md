https://gitlab.com/aovs/apostilas/PY-14

# Capitulo 1 - Curso de O.O

* Explicar que o curso será de O.O usando Python como suporte de linguagem
* docker run --rm -it -v $(pwd):/app python bash
* https://docs.python.org/3/


# Capítulo 2 - Conhecendo o Python

### Who is using?

* https://www.cleveroad.com/blog/discover-5-leading-companies-that-use-python-and-learn-does-it-fit-your-project
    * Instagram, Spotify, Facebook

* [Netflix](https://medium.com/netflix-techblog/python-at-netflix-bba45dae649e)

* [stackoverflow 2019](https://hipsters.tech/tendencias-em-tecnologia-pra-2020-hipsters-144/)
    * Primeiro em crescimento
    * Quem está usando quer continuar usando

* In 2018, it was the third most popular language on GitHub
    * Atrás somente de JavaScript e Java

### História

* 1991 - Linguagem O.O? Funcional? - Criada em  por [Guido Van](https://en.wikipedia.org/wiki/Guido_van_Rossum) como sucessora da linguagem ABC
    * Foco era não programador
    * durante as férias dele 1989
    * [Monty Python](https://en.wikipedia.org/wiki/Monty_Python%27s_Flying_Circus)
* 1995 - CNRI (Corporation for National Research Initiatives)  
* 2000 - BeOpen.com -  PythonLabs.
* 2001 - Zope Corporation
* 2002 - Direitos da Linguagem [PSF](https://www.python.org/psf/)


* Objetivo segundo o criador
    * An easy and intuitive language as understandable as plain English
    * Open source, so anyone can contribute to its development
    * Suitability for everyday tasks

* principais features
    * Listas
    * dicionários
    * identação obrigatória



### Compilação

* Compilada x Interpretada (PVM) - Python Virtual Machine
    * fazer analogia com Java
    * Jython (Programas python para bytecode Java) + IronPython

* Mas como compilar script Python?
    * falar que a compilação do Python está embutida
        * [CPython](https://en.wikipedia.org/wiki/CPython) - RI (escrito em C)
            * qual é o [melhor?](https://stackoverflow.com/questions/17130975/python-vs-cpython)
            * CPython is also the first to implement new features
            > CPython happens to be implemented in C. That is just an implementation detail, really. CPython compiles your Python code into bytecode (transparently) and interprets that bytecode in a evaluation loop.

        * [Pypy](https://en.wikipedia.org/wiki/PyPy) - uses JIT
            * PyPy often runs faster than CPython,
            > There is a project that does translate Python-ish code to C, and that is called [Cython](http://cython.org/). 

### Versões

* Retrocompatibilidade de versão 2x3

* PEP (python enhancement proposals)
    * analógoco JSR - JCP


### Nosso primeiro programa

* Modo Iterativo: Primeiro Programa (modo interativo x script)
    * abrir terminal e digita python3.6
    * print('olá mundo')

* Modo Programa: programa.py

### Curiosidades

* Compilação manual
    * import py_compile
    * py_compile.compile('programa.py)
    * python3 -m py_compile programa.py
    * pyc ==> bytecode python

* onde está o bytecode?
    * __pycache__

    > Toda vez que um script Python é executado, um código bytecode é criado. Se um script Python é importado como um módulo, o bytecode vai armazenar seu arquivo .pyc correspondente.

* não há ;
    > and the use of them is not considered “Pythonic. However, It is used to separate commands on a single line.

* [double or single quotes](https://stackoverflow.com/questions/56011/single-quotes-vs-double-quotes-in-python)

## Exercício 2.12


# Capítulo 3 - Variáveis

### Tipos das variáveis
* type('1'); type(1)
    * str, int, float
```python
<class 'str'>
```
* Complex Numbers
  * real + imaginárioa = 2 + 3j
* brincando com variáveis
  * idade+=1
  * ++idade
* python snake case
    * variáveis legais x ilegais
        * começar com número, @, palavras reservadas

![alt text](imgs/palavras_reservadas.png "reservadas")

### Conhecendo operadores

* Operadores aritméticos

![alt text](imgs/operadores.png "Operadores")

* Strings
  * 'rafa' * 3
  * concatenação
      * operador +
      * {}.format()
  * métodos
      * title(), capitalize(), upper()


### Recebendo dados

* input

```python
nome = input("digite seu nome:\n")
```

### Constantes

* True, False, None (diferente do Java, é um objeto, ocupa espaço de memória)
* bool()
> Repare que a função resulta False em strings vazias, quando um número é zero ou quando é None 

### Operadores de comparação

![alt text](imgs/operadores-2.png "Operadores")

* **is** != **==**
* == conteúdo
* is referência
```python
x = [1, 2, 3]
y = [1, 2, 3]
z = x

x == y #true
x is y #false
z is x #true
```

* Comando IF

```python
numero = 42
chute = input('Guess?')
if chute == numero:
    print('acertou')
else:
    print('errou')
```
* CONVERTENDO UMA STRING PARA INTEIRO
    * int(chute)
* Comando ELIF

## 3.12 - Exercício da Adivinhação

* comando **while**
    * aumentar tentativas

## 3.14 - Exercício da While

* comando **for**

```python
for i in range(0,10):
    print(i)
# O range(1, 10) vai gerar o intervalo de números inteiros de 1 a 9. 
```

* increments ==> steps

```python
for i in range(0,10,2):
    print(i)
```

## Exercícios 3.16 -  UTILIZANDO O FOR NO JOGO


# Capítulo 4 - Estrutura de Dados

* explicar o jogo da forca
  * palavra fixa **banana**
  * acertou e enforcou flags
* explicar a comparação de letras (case sensitive)
* como guardar os chutes feitos?

```python
chutes = []
type(chutes)
help(list)
```

* o que podemos fazer com uma lista?

    * len, min, max, in
    * [funções específicas](https://docs.python.org/3.6/library/stdtypes.html#mutable-sequence-types)

* melhorando a apresentacao
* Como finalizar?

```python
acertou = '_' not in letras_acertadas.
enforcou = erros == 6

```


## 4.1 - Exercícios - Jogo da Forca

```python
print('*********************************')
print('***Bem vindo ao jogo da Forca!***')
print('*********************************')
palavra_secreta = 'banana'
letras_acertadas = ['_', '_', '_', '_', '_', '_']
enforcou = False
acertou = False
erros = 0
print(letras_acertadas)

while(not enforcou and not acertou):

    if chute in palavra_secreta:
        posicao = 0
        for letra in palavra_secreta:
            if(chute.upper() == letra.upper()):
                letras_acertadas[posicao] = letra
            posicao = index + 1
    else:
        erros += 1
    
    enforcou = erros == 6
    acertou = '_' not in letras_acertadas
    print(letras_acertadas)
        
```

* DESAFIOS?
    * o que poderia melhorar?
    * mostrar palavras erradas
    * dar dicas?

### Sequências

* tipos
    * tuple, list, range, str
        * list é mutável
* operações
    * len, min, max, count, in

### Lists
* mutáveis, homogêneos
* indexável - 0 based
* índices negativos - valor na posição de forma reversa
* fatiamento

```python
lista = [2, 3, 5, 7, 11]
lista[0:2]
[2, 3]

#Se queremos todos os valores excluindo os dois primeiros, fazemos:
lista[2:]
[5, 7, 11]
```

* função append, extend == +
    * extend - mais de um elemento por vez: lista.extend(['dois', 'três',])


### Tuple

* lista imutável, heterogênea

```python
dias = ('domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sabado')
```
> Como são imutáveis, uma vez criadas não podemos adicionar nem remover elementos de uma tupla. O método append() da lista não existe na tupla:


### Range

* range(inicio, fim)

### Sets / Conjuntos

* não ordenados e sem repetição

* Operações em Sets
  *  (-diferença), | (uniao), & (interseccao), ^ (diferença simetrica)

```python
frutas = {'laranja', 'banana', 'uva', 'pera', 'laranja', 'uva', 'abacate'}
frutas = set()
```

> set() != {}; segundo cria dicionário vazio

### Dicionarios
* maps
* keys, values

```python
pessoa = {'nome': 'João', 'idade': 25, 'cidade': 'São Paulo'}
a = dict(um=1, dois=2, três=3)
pessoa.keys()
pessoa.values()
```
* Requisitos para as chaves
  * chaves de um dicionário não podem ser iguais para não causar conflito. 
  * Além disso, somente tipos de dados imutáveis podem ser usados como chaves,


## 4.5 - Exercício Estrutura de dados

# Capítulo 5 - Funções

* grupo de instruções que poderão ser reutilizadas
* funções built-ins
    * print, input, format, type

* **def** nome_da_funcao (parametros)

* função para calcular a idade
* explicar que a função pode returnar algo
* pode ter tipo de retorno

> sentir a vibe da turma, se tiver básica, fazer o algoritmo do calcular_idade antes e depois faz um refactoring extraindo para função

```python
import datetime

# parâmetro obrigatório
def calcular_idade(nascimento) -> int:
    return datetime.date.today().year - nascimento

print(calcular_idade(1985))

# parâmetro opcional
def calcular_idade(nascimento=1980) -> int:
    return datetime.date.today().year - nascimento

```

* função com mais de um parâmetro diferentes

```python
def mostrar_dados(nome, idade=None):
    pass

#ok
mostrar_dados('rafa', 34)
#ok
mostrar_dados('rafa')
#nao funciona corretamente - nome = 34
mostrar_dados(34)
#nao funciona - nome is required
mostrar_dados(idade=34)
```

### Retornando múltiplos valores

```python
def calculadora(x, y):
    return x+y, x-y
    return {'soma':x+y, 'subtração':x-y}

calculadora(1, 2)
>>> (3, -1) # retorna uma tupla

```


## Exercício 5.5 - criando nossas primeiras funções


### Vargs (**args* | ***kwargs** | * | **)

* *args
    * O *args  então é utilizado quando não sabemos de antemão quantos argumentos queremos passar para uma função.
  
```python
def teste(arg, *args):
    print('primeiro argumento normal: {}'.format(arg))
    for arg in args:
        print('outro argumento: {}'.format(arg))

# vargs pode receber lista
lista = ["é", "muito", "legal"]
teste('python', *lista)
# não esquecer do *
```

* **kwargs

* Map / Dictonary

```python
def minha_funcao(**kwargs):
    for key, value in kwargs.items():
        print('{0} = {1}'.format(key, value))

minha_funcao(nome='caelum')

# ou passar um dcionario
dicionario = {'nome': 'joao', 'idade': 25}
minha_funcao(**dicionario)
```


> A diferença é que o *args espera uma tupla de argumentos posicionais, enquanto o **kwargs um dicionário com argumentos nomeados.


## Exercício 5.8 - args e kwargs
## Exercício 5.9 - Função jogar no jogo da forca
* Importando a função jogar e chamando da console

### 5.10 - Módulos e Import

* import modulo (arquivo)
    * type (module)


# Capítulo 6 - Arquivos

* open
  * Ela recebe dois parâmetros: o primeiro é o nome do arquivo a ser aberto, e o segundo parâmetro é o modo que queremos trabalhar com esse arquivo - se queremos ler ou escrever.
  * modo append
    * 'a', de append: 
* write, close

```python

arquivo = open('palavras.txt', 'w')
arquivo.write('banana')
arquivo.close()

# append no arquivo
arquivo = open('palavras.txt', 'a')

```

## 6.8 - Exercício de leitura de arquivos

* Lendo dados do arquivo
    * se chamar o read 2x, o segundo não funciona
```python

#lendo o arquivo inteiro
palavra_secreta = arquivo.read()

#lendo linha por linha
print([linha for linha in arquivo])

#lendo apenas uma linha
arquivo.readline()

#autocloseable

with open('palavras.txt') as arquivo:
    for linha in arquivo:
        print(linha)

```
* Números aleatórios
  * random.randrange(0, 4)
  * número aleatório será usado para escolher uma palavra do array de palavras que estara no arquivo
  * 
  



* Defafio - palavras com spaces

```python

def jogar():
    imprime_mensagem_abertura()
    palavra_secreta = carrega_palavra_secreta()
    letras_acertadas = ["_" for letra in palavra_secreta]



#desafios
letras_acertadas.append('_') if not letra.isspace() else letras_acertadas.append('#')
```

## 6.11 - Exercício - Refatorando o programa - criando functions


> Tudo isso para melhorar legibilidade de código


# Capítulo 7 - Orientação a Objetos

* Descrever características
    * modela Conta como um dicionário (JSON)

  
```python
conta = {"numero": '123-4', "titular": "João", "saldo": 120.0, "limite": 1000.0}

```
* método constructor recebendo numero, titular, saldo, limite
* Descrever comportamentos, funcionalidades
    * deposita, saca, extrato
 
```python
#jeito tosco
def cria_conta():
    conta = {"numero": '123-4', "titular": "João", "saldo": 120.0, "limite": 1000.0}
    return conta

def cria_conta(numero, titular, saldo, limite):
    conta = {"numero": numero, "titular": titular, "saldo": saldo, "limite": limite}
    return conta

def deposita(conta, valor):
    conta['saldo'] = conta['saldo'] + valor

def extrato(conta):
    print("numero: {} \nsaldo: {}".format(conta['numero'], conta['saldo']))
```


## Exercício 7.2 - Criando uma conta

* Exercício 7.2 - 8 documentação

```python
    def deposita(self, valor):
        """método para depositar grana na Conta ."""
        self.__saldo += valor
    import (Conta)
    help(Conta)
```

### 7.3 - Començando classes e objetos

* explicar o que está RUIM --> RESPONSABILIDADE, ENCAPSULAMENTO
* vamos especificar o que é uma conta (atributos e comportamentos)
* consegue trocar o saldo da conta sem passar por saca e deposita
* criar classe e objeto
* criar Conta e definir atributos on the fly
```python
class Conta:
    pass
conta.titular = "João"
print(conta.titular)
```

* Como padronizar para todos os objetos terem os mesmos atributos da especifição?

### Constructor

```python
class Conta:
    def __init__(self, numero, titular, saldo, limite):
        self.numero = numero
        self.titular = titular
        self.saldo = saldo
        self.limite = limite

from conta import Conta
conta = Conta('123-4', 'João', 120.0, 1000.0)
conta.titular
```

> __new__ x __init__ - O método __new__() é realmente o construtor e é quem realmente cria uma
instância de Conta. O método __init__() é responsável por inicializar o objeto, tanto é que já
recebe a própria instância (self) criada pelo construtor como argumento

* tentar criar Conta sem parametros no constructor
* [Multiples Constructors](https://stackoverflow.com/questions/2164258/multiple-constructors-in-python)


```python
# default values
def __init__(self, city="Berlin"):
  self.city = city
```

* explicar que agora sao obrigatorios
* fazer desenho da memória


### Métodos
* criar métodos saca, deposita e extrato

```python
class Conta:
    def __init__(self, numero, titular, saldo, limite):
        self.numero = numero
        self.titular = tituar
        self.saldo = saldo
        self.limite = limite

    def deposita(self, valor):
        self.saldo += valor
    
    def saca(self, valor):
        self.saldo -= valor
    
    def extrato(self):
        print("numero: {} \nsaldo: {}".format(self.numero, self.saldo))

```
* criar uma conta, depositar, sacar e ver extrato

* return True or False para saca

```python
def saca(self, valor):
    if (self.saldo < valor):
        return False
    else:
        self.saldo -= valor
        return True
```

* criar duas contas e explicar referencias
  * == is
* criar c3 apontando para o mesmo objeto

> função **id()** para retornar o id da referência

* comparação ==

```python
c1 = Conta("123-4", "Python", 500.0, 1000.0)
c2 = Conta("123-4", "Python", 500.0, 1000.0)
if(c1 == c2):
    print(“contas iguais”)
>>> id(c1)
140059774918104
>>> id(c2)
140059774918104

```

* método transfere
    * procedural x O.O

* class Cliente **agregação**
* navegação com .

```python
from conta import Conta, Cliente
```

* class Historico **composição**

```python
import datetime
class Historico:
    def __init__(self):
        self.data_abertura = datetime.datetime.today()
        self.transacoes = []
    
    def imprime(self):
        print("data abertura: {}".format(self.data_abertura))
        print("transações: ")
    
        for t in self.transacoes:
            print("-", t)

class Conta:

    def __init__(self, numero, cliente, saldo, limite=1000.0):
        self.numero = numero
        self.cliente = cliente
        self.saldo = saldo
        self.limite = limite
        self.historico = Historico()
```

* em cada método da conta adicionar uma chamada ao historico.transacoes.append()


```python
def deposita(self, valor):
    self.saldo += valor
    self.historico.transacoes.append("depósito de {}".format(valor))

def saca(self, valor):
    if (self.saldo < valor):
        return False
    else:
        self.saldo -= valor
        self.historico.transacoes.append("saque de {}".format(valor))

def extrato(self):
    print("numero: {} \nsaldo: {}".format(self.numero, self.saldo))
    self.historico.transacoes.append("tirou extrato - saldo de {}".format(self.saldo))

conta1.historico.imprime()
```

### Métodos [dunder / magic](https://amontalenti.com/2013/04/11/python-double-under-double-wonder) methods

* dir(Conta) - listar métodos que a classe possui
* conta.__ __dict__ __ (retorna dicionário com atributos do **Objeto**)
* vargs(conta) - chama dict
* they simply mean, “reserved by the core Python team”

> The dunders achieve the dual goal of calling attention to these methods while also making them just the same as other plain methods in every aspect except naming convention.


## 7.13 - Exercício - Primeira clase Python


# Capítulo 8 - Modificadores de Acesso

* sacar acessando diretamento o atributo

* [Single x Double underscores](https://stackoverflow.com/questions/1301346/what-is-the-meaning-of-a-single-and-a-double-underscore-before-an-object-name)

* [Python public, protected, private](https://www.tutorialsteacher.com/python/private-and-protected-access-modifiers-in-python)

> Python doesn't have any mechanism that effectively restricts access to any instance variable or method. Python prescribes a convention of prefixing the name of the variable/method with single or double underscore to emulate the behaviour of protected and private access specifiers.

```python
self.__private = 'private'
self._protegido = 'protegido'
self.publico = 'publico'

class employee:
    def __init__(self, name, sal):
        self.__name=name

>>> e1=employee("Bill",10000)
>>> e1.__salary
AttributeError: 'employee' object has no attribute '__salary'

# Python performs name mangling of private variables. Every member with double underscore will be changed to _object._class__variable.
e1._Employee__salary

e1.__salary = 5000

#criado um novo atributo __salary
e1._Employee__salary (1000) 
```

* nothing is [really private](https://professormarcolan.com.br/en/how-to-do-encapsulation-in-python/)

    * it still is possible for a determined soul to access or modify a variable that is considered private.

```python    
# renomeou o atributo
print(conta._Conta__saldo)
```

* class Pessoa + atributo idade como privado
* pessoa.__idade = 10
    * explicar que ele não altera o atributo __idade, mas sim cria um novo atributo
    * pessoa._Pessoa__idade + __idade
* Como corrigir? [**__ __slots__ __**](http://python-history.blogspot.com/2010/06/inside-story-on-new-style-classes.html)

    * evitar que atributos sejam criados de forma dinâmica?
    * https://stackoverflow.com/questions/472000/usage-of-slots

> The special attribute __slots__ allows you to explicitly state which instance attributes you expect your object instances to have, with the expected results:

> The presence of __slots__ does several things. First, it restricts the valid set of attribute names on an object to exactly those names listed.

> Consumo de memória: Para contornar este problema é que se usa o __slots__ e este é seu principal propósito. O
__slots__
avisa o Python para não usar um dicionário e apenas alocar espaço para um conjunto fixo
de atributos.



```python
class Conta:
    __slots__ = ['_numero', '_titular', '_saldo', '_limite']
    def __init__(self, numero, titular, saldo, limite=1000.0):

# erro
conta.nome = "minha_conta"

```

### Encapsulamento - pega_saldo

* getters and setters

* maneira Java

```python
class Conta:
    def __init __(self, saldo):
        self._saldo = saldo
    
    def get_saldo(self):
        retorno self._saldo
    
    def set_saldo(self, saldo):
        self._saldo = saldo

# além de feio, quebra o encapsulamento
conta3.set_saldo(conta1.get_saldo() + conta2.get_saldo())

```

* Properties
    * decorators
    * posso criar a property para set saldo, mas faz sentido?

```python

@property
def saldo(self):
    return self._saldo

@saldo.setter
def saldo(self, saldo):
    if(self._saldo < 0):
        print("saldo não pode ser negativo")
    else:
        self._saldo = saldo

```

## Atributos de classe + Métodos estáticos

```python
class Conta:
    __total = 0

    # reparar que métodos estáticos não possuem o self
    @staticmethod
    def quantidade_contas():
        return Conta.__total

```

* métodos estáticos x métodos de classe

* [discussão longa](https://julien.danjou.info/guide-python-static-class-abstract-methods/)

```python
@classmethod
def get_total(cls):
    return cls._total

```

> Um método de classe pode mudar a implementação, ou seja, pode
ser reescrito pela classe filha. Já os métodos estáticos não podem ser reescritos pelas filhas, já que são
imutáveis e não dependem de um referência especial.
> 
>  Isso não é trivial. Métodos de classe servem para definir um método que opera na classe, e não em instâncias. Já os métodos estáticos utilizamos quando não precisamos receber a referência de um objeto especial (seja da classe ou de uma instância) e funciona como uma função comum, sem relação.


## 8.5 - Exercício


# Capítulo 9 - Pycharm

* explicações gerais de uma IDE x Vscode

## 9.9 - Exercício para criação de projetos


# CApítulo 10 - Herança x POlimorfismo

* esquema de funcionario

```python
class Gerente(Funcionario):
    def __init__(self, senha, qtd_funcionarios):
        Funcionario.__init__(nome, cpf, salario)
        #super().__init__(nome, cpf, salario)
        self._senha = senha
        self._qtd_funcionarios = qtd_funcionarios

```

## Override

*  get_bonificacao

* bonificação baseada na do funcionário

```python
def get_bonificacao():
    return super().get_bonificacao() + 1000
```

```python
class MinhaClasse(object):
    pass

print(dir(MinhaClasse()))
```

* __str__() e __repr__()==> menos comum de sobresceever (função eval())

* print(mc) --> __str__


> Para concluir, é importante entender que tanto __str__() quanto __repr__() retornam uma string que representa o objeto mas com propósitos diferentes. O método __str__() é utilizado para apresentar mensagens para os usuários da classe, de maneira mais amigável. Já o método __repr__() é usado para representar o objeto de maneira técnica, inclusive podendo utilizá-lo como comando válido como Python como vimos no exemplo da classe Ponto


* Outros dunder methods
    * __add__ - sobrescrita de operadores
        * [4,5] + [1,2]
        * https://docs.python.org/3/reference/datamodel.html#Basic_customization


### Poliformismo - Duck typing

* Controle de Bonificações

```python
class ControleDeBonificacoes:

    def __init__(self, total_bonificacoes=0):
        self._total_bonificacoes = total_bonificacoes

    def registra(self, funcionario):
        self._total_bonificacoes += funcionario.get_bonificacao()

    @property
    def total_bonificacoes(self):
        return self._total_bonificacoes
```

* O que impediria de passar um Cliente
    * **hasattr()** x **isinstance**

```python

cliente = Cliente('Maria', '333333333-33', '1234')
controle = ControleBonificacoes()
controle.registra(cliente)
#nada! daria erro


def registra(self, obj):

    # primeira opção
    if(hasattr(obj, 'get_bonificacao'))
        self._total_bonificacoes += obj.get_bonificacao()
    else
        print('instância de {} não implementa o método get_bonificacao()'.format(self.__class__.__name__))

    # segunda opçao
    if(isinstance(obj, Funcionario)):
        self.__total_bonificacoes += obj.get_bonificacao()
    

    # terceira opçao - maneira pythonica
    try:
        self._total_bonificacoes += obj.get_bonificacao()
    except AttributeError as e:
        print(e)
```

* Mas essa não é a maneira Pythônica. Você deve escrever o código esperando somente uma interface do objeto, não o tipo dele

* Duck Typing evita testes usando as funções type(), isinstance() e até mesmo a hasattr()- ao invés disso, deixa o erro estourar na frente do programador.
  * assumir a existência do atributo e capturar (tratar) um exceção quando o atributo não pertencer ao objeto e seguir o fluxo do programa. 
### Duck Typing

```python
#se parece como um pato e grasna como um pato, então deve ser um pato
class Pato:
    def grasna(self):
        print('quack!')

class Ganso:
    def grasna(self):
        print('quack!')

if __name__ == '__main__':
    pato = Pato()
    print(p.grasna())

    ganso = Ganso()
    print(g.grasna())

```

* Herança x Composição


## 10.7 - Exercício Herança e Poliimofiismo


### 10.8 Classes Abstratas

* objeto Funcionario nao faz sentido
* get_bonificacao as abstract

```python
import abc
class Funcionario(abc.ABC):
    @abc.abstractmethod
    def get_bonificacao(self):
        pass
```

## 10.9 - Exercício Classes Abstratas



# Capítulo 11 - Herança Múltipla e Interfaces

* método **autentica()** no Gerente + Diretor + Presidente
    * documentosSigilosos

* **FuncionarioAutenticavel**

* Diamond Problem
    * MRO --> method resolution order 

## 11.4 - Exercícios Mix-ins - Tributavel 

* Mixin X Módulo ABC
    

```python
import abc
class Autenticavel(abc.ABC):
"""
Classe abstrata que contém operações de um objeto autenticável.
Subclasses concretas devem sobrescrever o método
"""
    @abc.abstractmethod
    def autentica(self, senha):
        pass

    Autenticavel.register(Gerente)
    gerente = Gerente('João', '111111111-11', 3000.0)
    print(isinstance(Autenticavel)) #true
    print(issubclass(Autenticavel)) #true
```

> Mas qual a
diferença de herdar muitos mix-ins e muitas ABCs? Realmente, aqui não há grande diferença e voltamos
ao problema anterior dos mix-ins - muito acoplamento entre classes que gera a herança múltipla.

* método register

> O Python não vai verificar se existe uma implementação do método autentica em Gerente
quando registrarmos a classe.

* Tkinter - parte gráfica do modelo de classes


## 11.6 - Exercícios Mix-ins - Tributavel 

# Capítulo 12

* try / except / else / finally
* raise exception

# Capítulo 13

Conhecendo melhor as listas, tuplas, conjuntos, dicionarios

* módulo [collections](https://docs.python.org/3/library/collections.html)
* módulo collections.abc

* USERLIST, USERDICT E USERSTRING
    * namedtuple, defaultdict, counter, deque
        * defaultdict: não sobrescreve a chave do dict
        * Counter: contar ocorrências na estrutura
        * Deque: adicionar elementos no início e fim da fila
        * NamedTuple: nome da tupla e seus campos (imutável)

```python
Conta = namedtuple('Conta', 'numero titular saldo limite')
conta = Conta('123-4', 'João', 1000.0, 1000.0)
print(conta) # saída: Conta(numero='123-4', titular='João', saldo=1000.0, limite=1000.0)
print(conta.titular) #saída: João

```


```python
class MeuDicionario(UserDict):
    pass

# dicionário que só aceita str como key
class Pins(UserDict):
    def __contains__(self, key):
        return str(key) in self.keys()
    
    def __setitem__(self, key, value):
        self.data[str(key)] = value
```

> A classe UserDict não herda de dict mas simula um dicionário. A UserDict possui uma
instância de dict interna chamada data , que armazena os itens propriamente ditos
>
> Criar subclasses de tipos embutidos como dict ou list diretamente é propenso a erros porque seus métodos geralmente ignoram as versões sobrescritas.



```python

```
```python

```
```python

```
```python

```
```python

```


# Dicas

* data model

![alt text](imgs/data-model.png "Data Models")

* [Python x Ruby](https://learn.onemonth.com/ruby-vs-python/)

    * [Trends do StackOverflow](https://insights.stackoverflow.com/trends?tags=python%2Cruby%2Cjava%2Cphp%2Cgo)

> Should I learn Python or Ruby first?
Ruby saw a spike in popularity between 2010-2016, but it seems like the industry is trending towards Python. Here’s one way to help you make a decision: If you already have a specific client, job, or project lined up that requires you to know Ruby, learn Ruby. If not, learn Python first. Keep in mind there is a difference between Python 2 and Python 3. If you’re new to coding then I’d recommend you start with the latest version — Python 3