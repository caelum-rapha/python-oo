"""classe que especifica como a Conta funciona ."""
class Conta:
    _total = 0

    def __init__(self, titular):
        self.__titular = titular
        self.__saldo = 0
        self.publico = 'publico'
        self._protegido = 'protegido'
        Conta._total += 1


    def saca(self, valor):
        """saca dinheiro da conta ."""
        self.__saldo -= valor

    def deposita(self, valor):
        """método para depositar grana na Conta ."""
        self.__saldo += valor

    def transfere(self, destino, valor):
        self.saca(valor)
        destino.deposita(valor)

    @property
    def saldo(self):
        return self.__saldo

    @property
    def titular(self):
        return self.__titular

    @titular.setter
    def titular(self, titular):
        self.__titular = titular

    @staticmethod
    def quantidade_contas():
        return Conta._total

    @classmethod
    def get_total(cls):
        return cls._total
 