from banco.conta import Conta


def test_deve_depositar_valor_na_conta():
    conta = Conta("rafa")
    conta.deposita(150)
    assert conta.saldo == 150
    assert conta.publico is 'publico'
    assert conta._protegido is 'protegido'

    # quebrando encapsulamento
    assert conta._Conta__saldo == 150



def test_deve_sacar_valor_na_conta():
    conta = Conta("rafa")
    conta.deposita(150)
    assert conta.saldo == 150

    conta.saca(100)
    assert 50 == conta.saldo


def test_deve_mudar_titular_da_conta():
    conta = Conta("rafa")
    assert "rafa" == conta.titular

    conta.titular = "alterado"
    assert "alterado" == conta.titular


def test_deve_transferir_grana_da_conta():
    origem = Conta("rafa")
    destino = Conta("destino")

    origem.deposita(200)
    origem.transfere(destino, 50)

    assert 150 == origem.saldo
    assert 50 == destino.saldo


def test_deve_retornar_quantidade_de_contas():
    assert 5 == Conta.quantidade_contas()
    assert 5 == Conta.get_total()
