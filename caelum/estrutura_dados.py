from _curses import filter

lista = [12, -2, 4, 8, 29, 45, 12, 78, 36, -17, 2, 12, 8, 3, 3, -52]
print(max(lista))
print(min(lista))

print('*******************')
print('*******************')
print('*******************')
print('pares {}'.format([item for item in lista if item % 2 == 0]))

print('*******************')
print('*******************')
print('*******************')

print('Numero de vezes que o primeiro elemento aparece {}'.format(len([item for item in lista if item == lista[0]])))

print('*******************')
print('*******************')
print('*******************')

print(sum(lista) / len(lista))

print('soma total dos valores negativo {}'.format(sum([item for item in lista if item < 0])))

