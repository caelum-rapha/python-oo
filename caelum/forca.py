def jogar():
    """inicia jogo da forca."""

    print('*********************************')
    print('***Bem vindo ao jogo da Forca!***')
    print('*********************************')

    arquivo = open('palavras.txt')

    # print([linha for linha in arquivo])

    #ler o arquivo inteiro
    palavra_secreta = arquivo.read()

    # ler linha
    # palavra_secreta = arquivo.readline()

    arquivo.close();

    letras_acertadas = []

    for letra in palavra_secreta:

        letras_acertadas.append('_') if not letra.isspace() else letras_acertadas.append('#')

    enforcou = False
    acertou = False
    erros = 0
    print(letras_acertadas)

    while (not enforcou and not acertou):

        chute = input("Qual letra? ")
        chute = chute.strip()
        if chute in palavra_secreta:
            posicao = 0
            for letra in palavra_secreta:
                if chute.upper() == letra.upper():
                    letras_acertadas[posicao] = letra
                posicao = posicao + 1

        else:
            erros += 1

        enforcou = erros == 6
        acertou = '_' not in letras_acertadas
        print(letras_acertadas)

    if (acertou):
        print('voce ganhou')
    else:
        print('voce perdeu')

    print('fim')

jogar()