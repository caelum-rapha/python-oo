import psycopg2
from pypika import *


def conectar():
    try:
        connection = psycopg2.connect(user="postgres",
                                      password="",
                                      host="localhost",
                                      port="5432",
                                      database="python")

        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        print(connection.get_dsn_parameters(), "\n")

        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record, "\n")

        postgreSQL_select_Query = "select * from pessoa"

        executar(cursor, postgreSQL_select_Query)

        customers = Table('pessoa')

        q = Query.into(customers).columns('nome').insert('douglas123123').insert('rafa313123')

        cursor.execute(q.get_sql())

        connection.commit()

        print('******************depois****************')

        q = Query.from_('pessoa').select('id', 'nome').orderby('nome', order=Order.desc)

        executar(cursor, q.get_sql())




    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
    finally:
        # closing database connection.
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def executar(cursor, sql):
    cursor.execute(sql)
    print("Selecting rows from pessoa table using cursor.fetchall")
    mobile_records = cursor.fetchall()

    print("Print each row and it's columns values")
    for row in mobile_records:
        print("Id = ", row[0], )
        print("Name = ", row[1])


# Python program to use
# main for function call.
if __name__ == "__main__":
    conectar()
