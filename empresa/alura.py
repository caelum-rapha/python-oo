from empresa.funcionario import Funcionario


class Alura(Funcionario):
    def mostrar_tarefas(self):
        return 'Fez muita coisa, Alurete!'

    def busca_perguntas_sem_resposta(self):
        return 'Mostrando perguntas não respondidas do fórum'
