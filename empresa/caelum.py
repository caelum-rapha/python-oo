from empresa.funcionario import Funcionario


class Caelum(Funcionario):
    def mostrar_tarefas(self) -> str:
        return 'Fez muita coisa, Caelumer'

    def busca_cursos_do_mes(self, mes=None):
        return f'Mostrando cursos - {mes}' if mes else 'Mostrando cursos desse mês'
