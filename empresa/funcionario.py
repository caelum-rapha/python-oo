class Funcionario:

    def __init__(self, nome):
        self._nome = nome

    def registra_horas(self, horas):
        print('Horas registradas...')

    def mostrar_tarefas(self):
        print('Fez muita coisa...')

    @property
    def nome(self):
        return self._nome
