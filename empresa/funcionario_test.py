from empresa.junior import Junior
from empresa.pleno import Pleno
from empresa.senior import Senior


def test_deve_criar_funcionario_junior_e_pleno():
    jose = Junior('Jose')
    assert jose.mostrar_tarefas() == 'Fez muita coisa, Alurete!'

    matheus = Pleno('Matheus')

    assert matheus.mostrar_tarefas() == 'Fez muita coisa, Alurete!'

    assert matheus.nome == 'Matheus'

    ricardo = Senior('Ricardo')

    assert ricardo.mostrar_tarefas() == 'Fez muita coisa, Caelumer'

    assert str(ricardo) == 'Hipster, Ricardo'