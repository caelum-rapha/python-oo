# Chamamos estas classes pequenas, cujos objetos nem precisam ser instanciados, de Mixins
# https://en.wikipedia.org/wiki/Mixin
class Hipster:
    def __str__(self):
        return f'Hipster, {self._nome}'
