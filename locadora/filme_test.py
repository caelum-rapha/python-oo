from locadora.filme import Filme
from locadora.programa import Programa


def test_deve_construir_filme():
    batman = Filme("batman", 2018, 60)

    assert "Batman" == batman.nome
    assert 2018 == batman.ano
    assert 60 == batman.duracao

    assert batman.__str__() == "Sou o programa Batman do tipo Filme"


def test_deve_dar_likes():
    batman = Filme("batman", 2018, 60)

    batman.dar_likes()
    batman.dar_likes()
    batman.dar_likes()

    assert 6 == batman.likes


# https://devinpractice.com/2016/11/29/python-objects-comparison/

# == equals
# is ==
def test_devem_ser_filmes_iguais_pelo_nome():
    filme1 = Filme("batman", 2018, 60)
    filme2 = Filme("batman", 20180, 600)
    assert filme1.__eq__(filme2)
    assert filme1 is not filme2

    assert isinstance(filme1, Filme) is True
    assert isinstance(filme1, Programa) is True


def test_devem_ser_filmes_diferentes_pelo_nome():
    assert Filme("batman", 2018, 60).__eq__(Filme("batman", 2018, 60))
