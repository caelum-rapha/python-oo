from locadora.programa import Programa

class Locadora:
    def __init__(self) -> None:
        super().__init__()
        self.__playlist = []

    def adicionar(self, programa):
        self.__playlist.append(programa)

    # operator overloading
    def __add__(self, programa):
        if isinstance(programa, Programa):
            return self.adicionar(programa)

        raise Exception('So pode adicionar {}'.format(Programa.__class__.__name__))

    # duck typing
    # https://en.wikipedia.org/wiki/Duck_typing
    # https://stackoverflow.com/questions/20551042/whats-the-difference-between-iter-and-getitem
    def __getitem__(self, item):
        return self.__playlist[item]

    # Python Data Model.
    # https://docs.python.org/3/reference/datamodel.html
    def __len__(self):
        return len(self.__playlist)

