from locadora.filme import Filme
from locadora.locadora import Locadora
from locadora.serie import Serie
from locadora.programa import Programa
import pytest


def test_locadora_deve_ter_programas_adicionados():
    locadora = Locadora()
    locadora.adicionar(Filme('batman', 2010, 60))
    locadora.adicionar(Serie('lost', 2004, 6))

    locadora + Filme('vingadores', 2019, 120)

    assert len(locadora) is 3
    assert locadora[2].nome == 'Vingadores'


def test_nao_pode_adicionar_algo_que_nao_eh_programa_na_locadora():

    with pytest.raises(Exception):

        locadora = Locadora()
        locadora + Locadora()


def test_locadora_deve_ser_iteravel():
    locadora = Locadora()

    filme = Filme('batman', 2010, 60)
    serie = Serie('lost', 2004, 6)
    locadora + filme
    locadora + serie

    for programa in locadora:
        assert programa.nome is not None

    assert locadora[0] is filme
    assert locadora[1] is serie

    assert len(locadora) is 2

    assert id(locadora[0]) == id(filme)
    assert type(locadora[1]) == type(serie)

    #  For CPython, id(x) is the memory address where x is stored.
    print(id(filme))
    print(type(serie))
