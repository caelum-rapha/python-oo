# Isto significa que o atributo não está realmente privado, sendo acessível por meio de _Programa__nome
from abc import abstractmethod


class Programa:
    def __init__(self, nome, ano):
        self.__nome = nome.title()
        self.__ano = ano
        # Dependendo da situação, o caso pode ficar muito complexo; uma melhor opção é usar simplesmente um _ (underscore), assim oferecemos a ideia de protegido, sem fazermos o name mangling.
        self._likes = 0

    @abstractmethod
    def dar_likes(self):
        pass

    # mas diferentemente do java, poderia ter implementacao padrao
    @property
    def likes(self):
        return self._likes


    @property
    def nome(self):
        return self.__nome

    @nome.setter
    def nome(self, novo_nome):
        self.__nome = novo_nome.title()

    @property
    def ano(self):
        return self.__ano

    #  dunder methods

    # chamado pela referencia
    # __repr__, que é usado para mostrar uma representação que ajude no debug ou log de um código
    # O __repr__() é utilizado para demonstrar como o objeto foi criado, útil mais para o compilador do Python do que para o usuário fina
    # def __repr__(self) -> str:
    #     return self.__str__()

    # chamado pelo metodo print
    def __str__(self) -> str:
        return "Sou o programa {} do tipo {}".format(self.__nome, self.__class__.__name__)

    def __eq__(self, o: object) -> bool:
        if isinstance(o, self.__class__):
            return self.__nome == o.__nome
        return False

    def __hash__(self) -> int:
        return self.__nome.__hash__()
