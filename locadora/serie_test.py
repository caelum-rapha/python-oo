from locadora.serie import Serie


def test_deve_construir_serie():
    lost = Serie("game of thrones", 2018, 6)

    assert "Game Of Thrones" == lost.nome
    assert 2018 == lost.ano
    assert 6 == lost.temporadas

    lost.dar_likes()

    assert lost.likes == 1

    assert lost.__str__() == "Sou o programa Game Of Thrones do tipo Serie"

