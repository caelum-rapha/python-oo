def main():
    print('será que serei executado?')
print('e eu?')


#Python 2
#When Python runs the "source file" as the main program, 
#it sets the special variable (__name__) to have a value ("__main__").

# if __name__ == "__main__":
#     main()

#Python 3
# In Python 3, you do not need to use if__name. Following code also works

main()